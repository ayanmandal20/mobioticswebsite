// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueCarousel from 'vue-carousel'
import Routes from './routes'
import VueRouter from 'vue-router'
import { VLazyImagePlugin } from "v-lazy-image";
import style from './assets/style.css'

Vue.config.productionTip = false
Vue.use(VueCarousel);
Vue.use(VueRouter);
Vue.use(VLazyImagePlugin);
/* eslint-disable no-new */
const router = new VueRouter({
  mode: 'history',
  routes: Routes
});

export const eventBus = new Vue();

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router: router,
  style: style
})
