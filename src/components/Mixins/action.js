const action = {
    methods:{
        data(){
            this.filesubmit()
        },
        setFormData(dataset) {
            let submitData = new FormData();
            for(let i in dataset) {
                submitData.append(i, dataset[i]);
            }
            return submitData;
        },
        filesubmit(){
            let file = {}
            file.userid = this.name;
            file.email = this.email;
            file.mobile = this.mobile;
            file.msg = this.message;
            file.filename = this.docs;
            file.mbt_cntc_submit = ''
            let submitData = this.setFormData(file);
            this.actfileupload(submitData).then((response) => {
                console.log(response)
                let obj = {
                    id: file.userid,
                    email: file.email,
                    mobile: file.mobile,
                    msg: file.message
                }
                this.resumesubmit(response.success, obj)
            }).catch((error) => {
                console.log(error)
            })
        },
        resumesubmit(file, data){
            let resume = {}
            resume.userid = data.id;
            resume.email = data.email;
            resume.mobile = data.mobile;
            resume.msg = data.msg;
            resume.filename = file;
            resume.mbt_cntc_submit = ''
            console.log(resume)
            let formData = this.setFormData(resume)
            this.actContactForm(formData)
            // .then((res) => {
            //     console.log(res)
            // }).catch((error) => {
            //     console.log(error)
            // })
        },
        demoForm(){
           let obj =  {}
           obj.userid = this.name;
           obj.email = this.email;
           obj.mobile = this.mobile;
           obj.msg = this.message;
           obj.companytype = this.business;
           obj.mbt_cntc_submit = ''
            let submitData = this.setFormData(obj);
            this.actContactForm(submitData)
        },
        contactForm(){
            let objSet = {}
            objSet.userid = this.name;
            objSet.email = this.email;
            objSet.mobile = this.mobile;
            objSet.msg = this.message;
            objSet.mbt_cntc_submit = ''
            let submitData = this.setFormData(objSet);
            this.actContactForm(submitData)
        },
        actContactForm(submitData) {
            return new Promise((resolve,reject) =>{
                fetch('https://mobiotics.com/feedback.php', {
                    method: 'POST',
                    body: submitData
                }).then((response) => {
                    console.log(response)
                }).catch((error) => {
                    console.log(error)
                }); 
                this.name = ''
                this.email = ''
                this.mobile = ''
                this.message = ''
                this.business = ''
            })
        },
        actfileupload(formData){
            return new Promise((resolve,reject) =>{
                fetch('https://mobiotics.com/fileupload.php', {
                    method: 'POST',
                    body: formData
                }).then((response) => {
                    console.log(response)
                    return response.json()
                }).then((data) => {
                    resolve(data)
                }).catch((error) => {
                    reject(error)
                })
            })
        }
    }
}
export default action;