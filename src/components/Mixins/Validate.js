const utility = {
    methods:{
        validatename (info){
            this.errorsnames = [];
            if (!this.name) {
                this.errorsnames.push("Name Required");
                document.getElementById('bordername').style.borderColor = 'red'
                return false;
            }else if(this.name.trim() == ''){
                this.errorsnames.push("Blankspaces not allowed");
                document.getElementById('bordername').style.borderColor = 'red'
                return false;
            } 
            else if (!this.validName(this.name)){
                this.errorsnames.push('Valid Name Required');
                document.getElementById('bordername').style.borderColor = 'red'
                return false;
            } 
            document.getElementById('bordername').style.borderColor = 'gray'
            return true;
        },
        validName (name) {
            var na = /^[a-zA-Z ]{2,30}$/
            return na.test(name);
        },
        popupvalidatename (info){
            this.errorsnames = [];
            if (!this.name) {
                this.errorsnames.push("Name Required");
                document.getElementById('popupbordername').style.borderColor = 'red'
                return false;
            }else if(this.name.trim() == ''){
                this.errorsnames.push("Blankspaces not allowed");
                document.getElementById('popupbordername').style.borderColor = 'red'
                return false;
            } 
            else if (!this.validName(this.name)){
                this.errorsnames.push('Valid Name Required');
                document.getElementById('popupbordername').style.borderColor = 'red'
                return false;
            } 
            document.getElementById('popupbordername').style.borderColor = 'gray'
            return true;
        },
        validatemail (info) {
            this.errorsmails = [];
            if (!this.email) {
                this.errorsmails.push("Email Required");
                document.getElementById('borderemail').style.borderColor = 'red'
                return false;
            } else if (!this.validEmail(this.email)){
                document.getElementById('borderemail').style.borderColor = 'red'
                this.errorsmails.push('Valid Email Required');
                return false;
            } 
            document.getElementById('borderemail').style.borderColor = 'gray'
            return true;   
        },
        validEmail (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        popupvalidatemail (info) {
            this.errorsmails = [];
            if (!this.email) {
                this.errorsmails.push("Email Required");
                document.getElementById('popupborderemail').style.borderColor = 'red'
                return false;
            } else if (!this.validEmail(this.email)){
                document.getElementById('popupborderemail').style.borderColor = 'red'
                this.errorsmails.push('Valid Email Required');
                return false;
            } 
            document.getElementById('popupborderemail').style.borderColor = 'gray'
            return true;   
        },
        validatemobile (info) {
            this.errorsmobiles = [];
            if (!this.mobile) {
                this.errorsmobiles.push("Phone Required");
                document.getElementById('bordermobile').style.borderColor = 'red'
                return false;
            } else if (!this.validMobile(this.mobile)){
                this.errorsmobiles.push("Valid Phone Number Required");
                document.getElementById('bordermobile').style.borderColor = 'red'
                return false;
            } 
                document.getElementById('bordermobile').style.borderColor = 'gray'
                return true;
            
        },
        validMobile (mobile) {
            var mo = /^\d{10}$/;
            return mo.test(mobile);
        },
        popupvalidatemobile (info) {
            this.errorsmobiles = [];
            if (!this.mobile) {
                this.errorsmobiles.push("Phone Required");
                document.getElementById('popupbordermobile').style.borderColor = 'red'
                return false;
            } else if (!this.validMobile(this.mobile)){
                this.errorsmobiles.push("Valid Phone Number Required");
                document.getElementById('popupbordermobile').style.borderColor = 'red'
                return false;
            } 
                document.getElementById('popupbordermobile').style.borderColor = 'gray'
                return true;
            
        },
        validatemessage (info){
            this.errorsmessages = [];
            if (!this.message) {
                this.errorsmessages.push("Message Required");
                document.getElementById('bordermessage').style.borderColor = 'red'
                return false;
            }
                document.getElementById('bordermessage').style.borderColor = 'gray'
                return true;
        },
        popupvalidatemessage (info){
            this.errorsmessages = [];
            if (!this.message) {
                this.errorsmessages.push("Message Required");
                document.getElementById('popupbordermessage').style.borderColor = 'red'
                return false;
            }
                document.getElementById('popupbordermessage').style.borderColor = 'gray'
                return true;
        },
        validatebusiness (info){
            this.errorsbusinesses = [];
            if (!this.business) {
                this.errorsbusinesses.push("Business Required");
                document.getElementById('borderbusiness').style.borderColor = 'red'
                return false;
            }
                document.getElementById('borderbusiness').style.borderColor = 'gray'
                return true;
        }
    }
}

export default utility;