import mbt from './components/mbt.vue'
import About from './components/MainBody/About.vue'
import Clients from './components/MainBody/Clients.vue'
import Partners from './components/MainBody/Partner.vue'
import Products from './components/ProductPage/AllProducts.vue'
import AllTeam from './components/TeamPage/AllTeam.vue'


export default [
    {path:'/',name:'HOME', component: mbt},
    {path:'/#About', name:'ABOUT', component: About},
    {path:'/#Clients', name:'CLIENTS', component: Clients},
    {path:'/#Partners', name: 'PARTNERS', component: Partners},
    {path:'/Products',name:'PRODUCTS', component: Products},
    {path:'/Team',name:'TEAM', component: AllTeam}
]